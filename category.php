<?php
if (isset($_GET['catid'])){
    dispArt($_GET['catid']);
}else {
    dispArt(0);
}

echo "<a title='create article' class='new' href='index.php?new'>+</a>";

function dispArt($cat_id){
    include('db.php');
    
    if($cat_id == 0) {
        $sql = "SELECT article.art_title, art_id, user.user_name, category.cat_name, article.art_img FROM article, user, category WHERE (article.art_author_id = user.user_id) AND (article.art_cat_id = category.cat_id) ORDER BY art_id DESC";
    }else {
        $sql = "SELECT article.art_title, article.art_id, user.user_name, category.cat_name, article.art_img FROM article, user, category WHERE (article.art_author_id = user.user_id) AND (article.art_cat_id = category.cat_id) AND (category.cat_id = ".$cat_id.") ORDER BY art_id DESC";
    }
    
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) != 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $art_title = $row['art_title'];
            $art_id = $row['art_id'];
            $art_author = $row['user_name'];
            $cat_name = $row['cat_name'];
            $art_img = $row['art_img'];
            
            if (strlen($art_title) > 20) {
                $art_title = substr($art_title, 0, 18).'...';
            }

            echo "
            <div class='cat_art'>
                <div class='cat_art_img'><img class='art_img' src=".$art_img."></div>
                <div class='cat_art_title'><a href='index.php?artid=$art_id'>".$art_title."</a></div>
                <div class='cat_art_info'>by ".$art_author." / ".$cat_name."</div>
            </div>
        ";
        }
    }else {
        echo "No article yet...";
    }
}
?>

