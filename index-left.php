<div id='index-left'>
    <?php
    session_start(); 
    //已登入
    if (isset($_SESSION['username'])) {
        //顯示個人資料 登出按鈕
        echo "
            <div class='left-form'>
            Welcome,<br>".$_SESSION['username']."
            <form action='logout.php' method='GET'>
            <input type='submit' value='Logout'></input>
            </form>
            
            </div>
            ";
    }else { //未登入
        //顯示登入介面
        echo "
            <div class='left-form'>
            <form action='login-parser.php' method='post'>
            <span class='login-input'>Username</span>
            <input type='text' name='user_name' placeholder='username' required>
            <span class='login-input'>Password</span>
            <input type='password' name='user_password' placeholder='password' required>
            <input type='submit' name='login' value='LOGIN'>
            </form>
            <span class='login-register'>Don't have account? <a href='register.php'>Register</a></span>
            </div>
            ";
        //確認是否有任何狀態頁面 ex:login_failed
        if (isset($_GET['status'])) {
            if ($_GET['status'] == 'login_failed') {
                echo "<span class='login-failed'>Login failed, please try again...</span>";
            }
        }
    }
    ?>
</div>
