<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>CSS Design Web</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="category.css">
    <link rel="stylesheet" href="article.css">
</head>

<body>
    <!--head-->
    <?php include('index-top.php')?>

    <?php
    //確認是否有特殊頁面 reg_success
    if (isset($_GET['status'])){
        if ($_GET['status'] == 'reg_success') {
            echo '<p>Register Success!</p>';
            echo '<a href="/index.php">Back to Title.</a>';
        }
    }
    include('index-left.php');
    include('index-right.php');
    ?>
</body>

</html>
