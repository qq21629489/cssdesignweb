<?php
    include('db.php');
    session_start();

    if (isset($_POST['category']) && isset($_POST['title']) && isset($_POST['content']) && isset($_POST['code']) && isset($_POST['upload'])) {
        $cat = $_POST['category'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $code = $_POST['code'];
        
        //img upload setting
        $name = $_FILES['image']['name'];
        $target_dir = "upload/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);

        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif");

        // Check extension
        if( in_array($imageFileType,$extensions_arr) ){
            // Convert to base64 
            $image_base64 = base64_encode(file_get_contents($_FILES['image']['tmp_name']));
            $image = 'data:image/'.$imageFileType.';base64,'.$image_base64;
            // Insert record
            $sql = "INSERT INTO `article`(`art_cat_id`, `art_author_id`, `art_title`, `art_content`, `art_code`, `art_img`) VALUES (
            ".$_POST['category'].",
            (SELECT user_id FROM user WHERE user_name = '".$_SESSION['username']."'),
            '".$_POST['title']."',
            '".$_POST['content']."',
            '".$_POST['code']."', 
            '".$image."')";
            
            $result = mysqli_query($con, $sql);
            if ($result) {
                // Upload file
                move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$name);
                header("Location: index.php");
            }else {
                echo "Error: " . $sql . "" . mysqli_error($con);
            }
        }
    }else {
        echo "<script>alert('Error, please try again...');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
    }
?>