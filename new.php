<?php
    if (isset($_SESSION['username'])) {
        echo "  <form action='new-parser.php' method='POST' enctype='multipart/form-data'>
                <div class='art_title'>Category</div>
                <div class='art_content'><select name='category'>
                <option value='1'>Div</option>
                <option value='2'>Button</option>
                <option value='3'>Form</option>
                <option value='4'>Table</option>
                <option value='5'>Other</option>
                </select></div>
                
                <div class='art_title'>Title</div>
                <div class='art_content'><input type='text' name='title' required></div>
                
                <div class='art_title'>Author</div>
                <div class='art_content'>".$_SESSION['username']."</div>
                
                <div class='art_title'>Content</div>
                <div class='art_content'><textarea required name='content'></textarea></div>
                
                <div>
                <div class='art_title art_title_f'>Code</div>
                <div class='art_title art_title_s' onclick='run()'>Test</div>
                </div>
                <div class='art_content'><textarea id='editor' name='code'></textarea></div>
                
                <div class='art_title'>Preview</div>
                <iframe class='art_preview' id='iframe'></iframe>
                
                <div>
                <div class='art_title art_title_f'>Image</div>
                <div class='art_title art_title_s aaa'>Upload<input type='file' name='image' accept='image/*'></div>
                
                </div>
                
                <input class='upload' type='submit' name='upload' value='+' title='create article'>
                </form>";
    }else {
        echo "<script>alert('Please login before create new article!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
    }

    
?>
<html>
<link rel="stylesheet" href="codeMirror/lib/codemirror.css">
<link rel="stylesheet" href="codeMirror/theme/dracula.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="new.css">
<script src="codeMirror/lib/codemirror.js"></script>
<script src="codeMirror/mode/xml.js"></script>
<script src="codeMirror/mode/htmlmixed.js"></script>
<script>
    var editor = CodeMirror.fromTextArea(document.getElementById('editor'), {
        mode: 'xml',
        theme: 'dracula',
        htmlMode: true,
        lineNumbers: 'true',
    });

    run();

    function run() {
        document.getElementById('iframe').setAttribute('srcdoc', editor.getValue());
    }
</script>

</html>
