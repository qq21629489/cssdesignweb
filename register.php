<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>CSS Design Web Register</title>
    <link rel="stylesheet" href="style.css">
</head>

<style>
    #register-form {
        width: 300px;
        margin-top: 200px;
        padding: 10px;
        background-color: #969696;
        align-content: center;
        left: 50%;
        transform: translateX(-50%);
        position: absolute;
    }

    #register-form p {
        margin: 5px;
        padding: 5px;
    }

    #btn-register {
        float: right;
    }

</style>

<body>
    <?php include('index-top.php')?>
    <div id="register-form">
        <form action="register-parser.php" method="post">
            <p>Username: <input type="text" name="user_name" placeholder="username" required></p>
            <p>Password: <input type="password" name="user_password" placeholder="password" required></p>
            <?php
                if (isset($_GET['status'])){
                    if ($_GET['status'] == 'reg_failed'){
                        echo "
                        <p>username exists..</p>
                        ";
                    }
                }
            ?>
            <input id="btn-register" type="submit" name="register" value="Register">
        </form>
    </div>
</body>

</html>
