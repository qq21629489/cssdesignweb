<?php
include('index-top.php');


function getArtCode($art_id) {
    include('db.php');
    
    $sql = "SELECT article.art_code FROM article WHERE (article.art_id = ".$art_id.")";
    
    
    $result = mysqli_query($con, $sql);
    
    if (mysqli_num_rows($result) != 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            return $row['art_code'];
        }
    }
}
    
function showCode($code) {
    echo $code;
}
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>CSS Design Web Try It</title>
    <link rel="stylesheet" href="codeMirror/lib/codemirror.css">
    <link rel="stylesheet" href="codeMirror/theme/dracula.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="tryit.css">
    <script src="codeMirror/lib/codemirror.js"></script>
    <script src="codeMirror/mode/xml.js"></script>
    <script src="codeMirror/mode/htmlmixed.js"></script>
</head>

<body>
    <div id='editorWrapper'>
        <textarea id='editor'><?php
if (isset($_GET['artid'])) echo getArtCode($_GET['artid']);?></textarea>
    </div>
    <div id='iframeWrapper'>
        <iframe id='iframe'></iframe>
    </div>

    <button id='run' onclick="run()">Run</button>
</body>

<script>
    var editor = CodeMirror.fromTextArea(document.getElementById('editor'), {
        mode: 'xml',
        theme: 'dracula',
        htmlMode: true,
        lineNumbers: 'true',
    });

    function run() {
        document.getElementById('iframe').setAttribute('srcdoc', editor.getValue());
    }

    window.onload = function() {
        run();
    }

</script>

</html>
